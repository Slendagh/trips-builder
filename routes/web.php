<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get("/", ['uses' => 'App\Http\Controllers\FlightController@index']);
Route::post("/searchFlights", ['uses' => 'App\Http\Controllers\FlightController@searchFlights', 'as'=>'searchFlights']);
Route::post("/nextStep", ['uses' => 'App\Http\Controllers\FlightController@nextStep']);
Route::post("/endOfRoundTrip", ['uses' => 'App\Http\Controllers\FlightController@endOfRoundTrip']);
Route::post("/sort", ['uses' => 'App\Http\Controllers\FlightController@sort']);

