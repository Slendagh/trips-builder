
@extends ('layout.layout')
@section('content')
<style>
    .flexrow{
        display:flex;
        flex-direction: row;
        justify-content: space-between;
    }
    .flexcolumn{
        display:flex;
        flex-direction:column;
    }
    .justify_between{
        justify-content: space-between;
    }
    .container{
        width:70%;
        margin:auto;
        padding:2rem;
    }
    #results{
        margin:20px;
    }
    .error{
        color:red;
        margin:20px;
    }
    #returns{
        display:none;
    }
    .h-90{
        height:90px;
    }
    .align-center{
        align-items: center;
    }
    #flight{
        padding:20px;
        margin: 10px 0px;
    }
    form{
        margin:20px;
    }
    .input{
        border: 1px solid black;
        width: 210px;
    }
    .search{
        margin: auto;
        display: flex;
        margin-top: 20px;
        width: 100px;
    }
    .border{
        border: 1px solid;
    }
    .preferred_airline{
        margin:auto;
        padding-top:10px;
        width:50%;
    }
</style>

<div class="container">
    <form method="POST" action="{{route('searchFlights')}}">
        {{ csrf_field() }}
        <!--Select departure ariport-->
        <label for="departure_airport">From </label>
        <input class="input" list="airports_departure" name="departure_airport"/>
        <datalist id="airports_departure">
            @foreach($airports as $airport)
                <option value="{{$airport->city.', '.$airport->region_code.', '.$airport->country_code.' - '.$airport->name.' ['.$airport->code.']'}}">
            @endforeach
        </datalist>

        <!--Select arrival airport -->
        <label for="arrival_airport">To </label>
        <input class="input" list="airports_arrival" name="arrival_airport" />
        <datalist id="airports_arrival">
            @foreach($airports as $airport)
                <option value="{{$airport->city.', '.$airport->region_code.', '.$airport->country_code.' - '.$airport->name.' ['.$airport->code.']'}}">
            @endforeach
        </datalist>

        <label for="departure_date">Depart </label>
        <input name="departure_date" id="departure_date" type="date" placeholder="2022-01-22">

        <label for="return_date">Return </label>
        <input name="return_date" id="return_date" type="date" placeholder="2022-01-27">

        <select class="border" name="trip_type">
            <option value="one_way">One way</option>
            <option value="round_trip">Round trip</option>
        </select>

        <div class="preferred_airline">
            <label for="preferred_airline"><span style="color:gray">(Optional)</span> select preferred airline  </label>
            <input class="input" list="airlines" name="preferred_airline"/>
            <datalist class="border" id="airlines">
                @foreach($airlines as $airline)
                    <option value="{{$airline->name}}">
                @endforeach
            </datalist>
        </div>
        <input type="submit" class="search border" value="search">
    </form>

    @if(session('error'))
    <div class="error">
        {{session('error')}}
    </div>
    @endif

    <div id="results">
        @if(session('AB_flights'))
            <div class="flexrow">
                <div>Please select your departure flight </div>
                <select class="border" id="sort" onchange="sort({{session()->get('AB_flights')}}, 'departure', null)">
                    <option value="no_sort">No sort</option>
                    <option value="sort_price_desc">Sort by price (High to low)</option>
                    <option value="sort_price_asc">Sort by price (Low to high)</option>
                </select>
            </div>

            <div class="flexcolumn justify_between" id="flights">
                @foreach(session('AB_flights') as $AB_flight)
                    <div class="flexrow h-90 align-center border" id="flight">
                        <div class="flexrow">
                            <div>{{$AB_flight['airline']}}</div>
                            <div>&nbspFlight {{$AB_flight['number']}}</div>
                        </div>
                        <div class="flexrow">
                            <div>{{$AB_flight['departure_time']}}</div>
                            <div>-</div>
                            <div>{{$AB_flight['arrival_time']}}</div>
                        </div>
                        <div class="flexrow">
                            <div>{{$AB_flight['departure_airport']}}</div>
                            <div>-</div>
                            <div>{{$AB_flight['arrival_airport']}}</div>
                        </div>
                        <div class="flexrow">
                            <div>${{$AB_flight['price']}}</div>
                        </div>
                        <input class="border" id="selectFlight" type="button" value="select this flight" onclick="selectDeparture({{$AB_flight['id']}})">
                    </div>
                 @endforeach
            </div>
            <div id="tripType" data-tripType='{{ session()->get('trip_type') }}'></div>
            <div id="departureDate" data-departureDate='{{ session()->get('departure_date') }}'></div>
            <div id="returnDate" data-returnDate='{{ session()->get('return_date') }}'></div>
        @endif  
    </div>
</div>



<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script> 

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
     
      
    function selectDeparture(id){

        var trip_type = document.getElementById('tripType').dataset.triptype;
        var departure_date = document.getElementById('departureDate').dataset.departuredate;
        var return_date = document.getElementById('returnDate').dataset.returndate;

        $.ajax({
            type: 'POST',  
            url: '/nextStep', 
            data:  {'id':id, 'trip_type':trip_type, 'departure_date':departure_date, 'return_date':return_date},
            dataType: 'json',
            success: function(response) {
                if(response.includes("Congratulations")){
                    document.getElementById("results").innerHTML = response;
                }else if (response.length == 0){
                    document.getElementById("results").innerHTML = "There are no returning Flights";
                }else{
                    document.getElementById("results").innerHTML = "<div class='flexrow'><div>Please Select your returning flight </div><select class='border' id='sort' onchange='sort("+JSON.stringify(response)+",\"returns\","+id+")'><option value='no_sort'>No sort</option><option value='sort_price_desc'>Sort by price (High to low)</option><option value='sort_price_asc'>Sort by price (Low to high)</option></select></div>";
                    document.getElementById("results").innerHTML +="<div class='flexcolumn justify_between' id='flights'>";
                    for(var i=0; i<response.length; i++){
                        console.log(response[i].id)
                        document.getElementById("flights").innerHTML+="<div class='flexrow h-90 align-center border' id='flight'><div class='flexrow'><div>"+response[i]['airline']+"</div><div>&nbspFlight "+response[i]['number']+"</div></div><div class='flexrow'><div>"+response[i]['departure_time']+"</div><div>-</div><div>"+response[i]['arrival_time']+"</div></div><div class='flexrow'><div>"+response[i]['departure_airport']+"</div><div>-</div><div>"+response[i]['arrival_airport']+"</div></div><div class='flexrow'><div>$"+response[i]['price']+"</div></div><input class='border' id='endOfTrip' type='button' value='select this returning flight' onclick='selectReturn("+id+", "+response[i]['id']+")'></div>"
                    }
                    document.getElementById("results").innerHTML +="</div></div><div id='tripType' data-tripType=''></div><div id='returnDate' data-returnDate="+return_date+"></div><div id='departureDate' data-departureDate="+departure_date+"></div>"
                }
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log('Error - ' + errorMessage);
            }
        });
    }

    function selectReturn(A, B){
        var trip_type = document.getElementById('tripType').dataset.triptype;
        var departure_date = document.getElementById('departureDate').dataset.departuredate;
        var return_date = document.getElementById('returnDate').dataset.returndate

        $.ajax({
            type: 'POST',  
            url: '/endOfRoundTrip', 
            data:  {'A':A, 'B':B, 'trip_type':trip_type, 'departure_date':departure_date, 'return_date':return_date},
            dataType: 'text',
            success: function(response) {
                document.getElementById("results").innerHTML = response;
            },
            error: function(xhr, status, error){
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log('Error - ' + errorMessage);
            }
        });
    }


    function sort(flights, type, id){
        if(document.getElementById('sort')){
            var e = document.getElementById('sort');
            var selected_sort = e.options[e.selectedIndex].value;
            $.ajax({
                type: 'POST',  
                url: '/sort', 
                data:  {'selected_sort':selected_sort, 'flights':flights},
                dataType: 'json',
                success: function(response) {
                    document.getElementById("flights").innerHTML = "";
                    for(var i=0; i<response.length; i++){
                        if(type == "returns" && id !== null){
                            document.getElementById("flights").innerHTML += "<div class='flexrow h-90 align-center border' id='flight'><div class='flexrow'><div>"+response[i]['airline']+"</div><div>&nbspFlight "+response[i]['number']+"</div></div><div class='flexrow'><div>"+response[i]['departure_time']+"</div><div>-</div><div>"+response[i]['arrival_time']+"</div></div><div class='flexrow'><div>"+response[i]['departure_airport']+"</div><div>-</div><div>"+response[i]['arrival_airport']+"</div></div><div class='flexrow'><div>$"+response[i]['price']+"</div></div><input class='border' id='endOfTrip' type='button' value='select this returning flight' onclick='selectReturn("+id+", "+response[i]['id']+")'>";
                        }
                        if(type == "departure" && id == null){
                            document.getElementById("flights").innerHTML += "<div class='flexrow h-90 align-center border' id='flight'><div class='flexrow'><div>"+response[i]['airline']+"</div><div>&nbspFlight "+response[i]['number']+"</div></div><div class='flexrow'><div>"+response[i]['departure_time']+"</div><div>-</div><div>"+response[i]['arrival_time']+"</div></div><div class='flexrow'><div>"+response[i]['departure_airport']+"</div><div>-</div><div>"+response[i]['arrival_airport']+"</div></div><div class='flexrow'><div>$"+response[i]['price']+"</div></div><input class='border' id='selectFlight' type='button' value='select this flight' onclick='selectDeparture("+response[i]['id']+")'>";
                        }
                    }
                },
                error: function(xhr, status, error){
                    var errorMessage = xhr.status + ': ' + xhr.statusText
                    console.log('Error - ' + errorMessage);
                }
            })
        }
    }
</script> 


@endsection