
## To provision the environment, install and run the application on Windows

- Download PHP v-8.1.2 
- Remove the semicolon from php.ini file in lines ";extension=mbstring" and ";extension=openssl"
- Install composer

## About Trip Builder web services

- Trip Builder allows you build your next trip. Besides the ability to select the departure location, destination, trip type and travel date(s). You also have the possibility to search per your preferred airline and to sort the results by price in both ascending and descending order.

- You can access website at http://young-wildwood-52697.herokuapp.com/
