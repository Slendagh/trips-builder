<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('flights', function (Blueprint $table) {
            $table->id();
            $table->string("airline");
            $table->foreign("airline")->references("code")->on("airlines");
            $table->string("number");
            $table->string("departure_airport");
            $table->foreign("departure_airport")->references("code")->on("airports");
            $table->string("departure_time");
            $table->string("arrival_airport");
            $table->foreign("arrival_airport")->references("code")->on("airports");
            $table->string("arrival_time");
            $table->string("price");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('flights');
    }
}
