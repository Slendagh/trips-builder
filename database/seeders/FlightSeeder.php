<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Flight;

class FlightSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $flights = [
            /*[
                "airline" => "AC",
                "number" => "301",
                "departure_airport" => "YUL",
                "departure_time" => "07:35",
                "arrival_airport" => "YVR",
                "arrival_time" => "10:05",
                "price" => "273.23"
            ],
            [
                "airline" => "AC",
                "number" => "302",
                "departure_airport" => "YVR",
                "departure_time" => "11:30",
                "arrival_airport" => "YUL",
                "arrival_time" => "19:11",
                "price" => "220.63"
            ]
            [
                "airline" => "UA",
                "number" => "870",
                "departure_airport" => "AKL",
                "departure_time" => "09:45",
                "arrival_airport" => "YUL",
                "arrival_time" => "16:32",
                "price" => "3273.23"
            ]
            [
                "airline" => "AC",
                "number" => "303",
                "departure_airport" => "YVR",
                "departure_time" => "23:30",
                "arrival_airport" => "YUL",
                "arrival_time" => "07:11",
                "price" => "200.63"
            ]*/
           
        ];

        foreach ($flights as $flight) {
            Flight::create($flight);
        }
    }
}
