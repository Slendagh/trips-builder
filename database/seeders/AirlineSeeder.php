<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Airline;

class AirlineSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $airlines = [
            /*[
                 "code" => "AC",
                "name" => "Air Canada"
            ]
            [
                "code" => "UA",
                "name" => "United Airlines"
            ]*/
           
        ];

        foreach ($airlines as $airline) {
            Airline::create($airline);
        }

    }
}
