<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Flight;
use App\Models\Airport;
use App\Models\Airline;
use Carbon\Carbon;

class FlightController extends Controller
{

    public function index(){

        $airports = Airport::all();
        $airlines = Airline::all();
        return view('index', ['airports' => $airports, 'airlines' => $airlines]);
    }

   
    //gets a request->search database for flights that corresponds to the request's details->return all flights available or errors
    public function searchFlights(Request $request)
    {
        //inputs check
        if($request["departure_airport"] == null){
            return redirect()->action([FlightController::class, 'index'])->with('error', 'Please select departure airport');
        }else if($request["arrival_airport"] == null ){
            return redirect()->action([FlightController::class, 'index'])->with('error', 'Please select return airport');
        }else if($request['departure_date'] == null){
            return redirect()->action([FlightController::class, 'index'])->with('error', 'Please select departure date');
        }else if($request['trip_type']== "round_trip" && $request['return_date'] == null){
            return redirect()->action([FlightController::class, 'index'])->with('error', 'Please select return date');
        }

        //check if the departure date is after the retrurn date
        if($request["trip_type"] == "round_trip"){
            if(Carbon::parse($request["departure_date"])->gt(Carbon::parse($request["return_date"]))){
                $error = "The return date should be on or after the departure date";
                $AB_flights = null;
            }
        }

        //usefull variables declarations
        $error ="";
        $codeDeparture = explode("[", $request["departure_airport"]);
        $codeDeparture = explode("]", $codeDeparture[1] );
        $codeDeparture = $codeDeparture[0];

        $codeArrival = explode("[", $request["arrival_airport"]);
        $codeArrival = explode("]", $codeArrival[1] );
        $codeArrival = $codeArrival[0];

        $timezone = Airport::where("code", '=', $codeDeparture)->pluck('timezone')->first();
        $AB_flights ="";

        //A -> B 
        //check on departure date
        //check if the departure date is more than a year from now
        //check if the departure day no longer exists in the departure airport timezone
        if(Carbon::parse($request["departure_date"])->diffInDays(Carbon::now($timezone)->startOfDay()) > 365 ){
            $error = "Departure date shouldn't be more than a year from now";
        }elseif(Carbon::parse($request["departure_date"])->startOfDay()->format("m/d/y") < Carbon::now($timezone)->startOfDay()->format("m/d/y")){
            $error = "You can't select flights from the past";
        }else{
            //search for flights
            //if passenger has preferred airline=>search accordingly
            if($request['preferred_airline']){
                $AB_flights = Flight::where([
                    ["departure_airport", "=", $codeDeparture],
                    ["arrival_airport", "=", $codeArrival],
                ])->whereHas('airline_relation', function($query) use($request) {
                    $query->where("name", '=', $request['preferred_airline']);
                })->with('airline_relation')->with('departure_airport_relation')->with('arrival_airport_relation')->get();
            }else{
                 $AB_flights = Flight::where([
                    ["departure_airport", "=", $codeDeparture],
                    ["arrival_airport", "=", $codeArrival]
                ])->with('airline_relation')->with('departure_airport_relation')->with('arrival_airport_relation')->get();
            }
            
            //if no flight were found->show error, else calculate duration time and add it to the flight(s) collection
            if($AB_flights->isEmpty()){
                $error = "No flights available";
                $AB_flights = null;
            }else{
                //calculate flight duration
                foreach($AB_flights as $AB_flight){
                    $AB_flight["flight_duration"] = $AB_flight->flight_duration();
                }
            }

            //check on departure time
            //if passenger wants to travel on same day as trip creation, check if there is still time to catch the flight
            if(Carbon::parse($request["departure_date"])->startOfDay()->format("m/d/y") == Carbon::now($timezone)->StartOfDay()->format("m/d/y") && $AB_flights !== null){
                $results = [];
                $exists = false;
                Foreach($AB_flights as $flight){
                    if(Carbon::now($flight->departure_airport_relation->timezone)->setSeconds(0)->toTimeString() < Carbon::parse($flight->departure_time)->toTimeString())
                    {
                        array_push($results, $flight);
                        $exists = true;
                    }
                }
                if($exists == true){
                    $AB_flights = collect($results);
                }else{
                    $error ="No flights available";
                    $AB_flights = null;
                }
            }
        }
        //end A->B

        return redirect()->action([FlightController::class, 'index'])->with('AB_flights', $AB_flights ?? '')->with('departure_date', $request['departure_date'])->with('return_date', $request['return_date'])->with('trip_type', $request["trip_type"])->with('error', $error??'');
    }


    //if it's a round-trip->searches for returning flight for the corresponding departure flight selected, else send success message
    public function nextStep(Request $request)
    {
        //get the selected departure flight object
        $AB_flight = Flight::where('id', '=', $request['id'])->with('departure_airport_relation')->with('arrival_airport_relation')->get();
        
        if($request['trip_type'] == "round_trip" ){
            $BA_flights = Flight::where([
                            ['departure_airport', '=', $AB_flight[0]->arrival_airport],
                            ['arrival_airport', '=', $AB_flight[0]->departure_airport],
                        ])->with('airline_relation')->with('departure_airport_relation')->with('arrival_airport_relation')->get();
            
            //check what returning flight(s) are departing after the arrival of the departure flight selected, then store them in array
            $results = [];
            foreach($BA_flights as $BA_flight){
                if($BA_flight->departure_full_date($request["return_date"]) > $AB_flight[0]->arrival_full_date($request['departure_date'])){
                    array_push($results, $BA_flight);
                }
            }
            $BA_flights = $results;

            return collect($BA_flights);
        }
        return json_encode("Congratulations, your seat is reserved!<br>You payed a total of $".$AB_flight[0]->price."<br>Flight ".$AB_flight[0]->airline."".$AB_flight[0]->number." from ".$AB_flight[0]->departure_airport." ".$AB_flight[0]->departure_full_date($request['departure_date'])." (".$AB_flight[0]->departure_airport_relation->city.") to ".$AB_flight[0]->arrival_airport." ".$AB_flight[0]->arrival_full_date($request['departure_date'])." (".$AB_flight[0]->arrival_airport_relation->city.")");
    }


    //generates and send a success message after selecting both the departure and the returning flight
    public function endOfRoundTrip(Request $request){
        $AB_flight = Flight::where('id', '=', $request['A'])->with('departure_airport_relation')->with('arrival_airport_relation')->get();
        $BA_flight = Flight::where('id', '=', $request['B'])->with('departure_airport_relation')->with('arrival_airport_relation')->get();

        $text = "Congratulations, your seats are reserved!<br>";
        $text.= "You payed a total of $".intVal($AB_flight[0]->price) + intVal($BA_flight[0]->price)."<br>";
        $text.= "Flight ".$AB_flight[0]->airline."".$AB_flight[0]->number." from ".$AB_flight[0]->departure_airport." ".$AB_flight[0]->departure_full_date($request['departure_date'])." (".$AB_flight[0]->departure_airport_relation->city.") to ".$AB_flight[0]->arrival_airport." ".$AB_flight[0]->arrival_full_date($request['departure_date'])." (".$AB_flight[0]->arrival_airport_relation->city.")";
        $text.= "<br>Flight ".$BA_flight[0]->airline."".$BA_flight[0]->number." from ".$BA_flight[0]->departure_airport." ".$BA_flight[0]->departure_full_date($request['departure_date'])." (".$BA_flight[0]->departure_airport_relation->city.") to ".$BA_flight[0]->arrival_airport." ".$BA_flight[0]->arrival_full_date($request['return_date'])." (".$BA_flight[0]->arrival_airport_relation->city.")";
        
        return $text;
    }

    //sorts flights by price, both asc and desc
    public function sort(Request $request){
        $flights = collect($request['flights']);

        if($flights->count() > 1){
            if($request['selected_sort'] == "sort_price_asc"){
                $sorted_flights = $flights->sortBy('price');
                return $sorted_flights->values()->all();

            }else if($request['selected_sort'] == "sort_price_desc"){
                $sorted_flights = $flights->sortByDesc('price');
                return $sorted_flights->values()->all();
            }
        }

        return $flights;
    }
}
