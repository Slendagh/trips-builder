<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Airline;
use App\Models\Airport;
use Carbon\Carbon;

class Flight extends Model
{
    use HasFactory;
    
    public $timestamps = false;

    public function airline_relation()
    {
        return $this->belongsTo(Airline::class, 'airline', 'code');
    }

    public function departure_airport_relation()
    {
        return $this->belongsTo(Airport::class, 'departure_airport', 'code');
    }

    public function arrival_airport_relation()
    {
        return $this->belongsTo(Airport::class, 'arrival_airport', 'code');
    }

    public function flight_duration()
    {
        $arrival_time= Carbon::createFromFormat('H:i', $this->arrival_time, $this->arrival_airport_relation->timezone);
        $arrival_time = $arrival_time->setTimezone($this->departure_airport_relation->timezone);
        $departure_time= Carbon::createFromFormat('H:i', $this->departure_time, $this->departure_airport_relation->timezone);
        $flight_duration = intVal($arrival_time->diffInMinutes(Carbon::parse($departure_time)));
        if($flight_duration >= 60){
            $flight_duration = $flight_duration/60;
            return $flight_duration;
        }
        return $flight_duration;
    }

    public function flight_duration_minutes(){
        $arrival_time= Carbon::createFromFormat('H:i', $this->arrival_time, $this->arrival_airport_relation->timezone);
        $arrival_time = $arrival_time->setTimezone($this->departure_airport_relation->timezone);
        $departure_time= Carbon::createFromFormat('H:i', $this->departure_time, $this->departure_airport_relation->timezone);
        $flight_duration = intVal($arrival_time->diffInMinutes(Carbon::parse($departure_time)));
        return $flight_duration;
    }

    //Arrival full date calculation: departure full date+flightDuration => converted to arrival airport timezone
    public function arrival_full_date($date)
    {
        $arrival_full_date = Carbon::createFromFormat('Y-m-d H:i', $date." ".$this->departure_time, $this->departure_airport_relation->timezone)->addMinutes($this->flight_duration_minutes());
        return $arrival_full_date->setTimezone($this->arrival_airport_relation->timezone);
    }

    public function departure_full_date($date)
    {
        $arrival_full_date = Carbon::createFromFormat('Y-m-d H:i', $date." ".$this->departure_time, $this->departure_airport_relation->timezone);
        return $arrival_full_date;
    }

    //accessor for price
    public function getPriceAttribute($value){
        return intval($value);
    }
}
